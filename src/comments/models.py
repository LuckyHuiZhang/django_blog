from django.conf import settings
from django.db import models

class Comment(models.Model):
	user        = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)
	url         = models.URLField()
	content     = models.TextField()
	#image       = models.ImageField()
	allow_annon = models.BooleanField(default=True)
	timestamp   = models.DateTimeField(auto_now_add=True)
	updated     = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.url
