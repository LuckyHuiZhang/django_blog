from django.contrib import admin

# Register your models here.
from .models import BlogPost

class PostModelAdmin(admin.ModelAdmin):
	list_display = ["title", "updated", "timestamp"]
	list_display_links = ["title", "updated"]
	list_aditable = ["title"]
	list_filter = ["updated", "timestamp"]
	search_fields = ["title", "content"]
	class Mate:
		medel = BlogPost



admin.site.register(BlogPost, PostModelAdmin)


