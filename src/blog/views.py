
from django.conf import settings
from django.views.generic import TemplateView

from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.http import Http404
from django.shortcuts import render, get_object_or_404, redirect

# Create your views here.
from .forms import BlogPostModelForm
from .models import BlogPost


# get -> 1 object #
# filter -> [] objects #

# search by <int:id> #
# def blog_post_detail_page(request, post_id):
# 	try:
# 	    obj = BlogPost.objects.get(id=post_id)#query -> database -> data -> django renders it
# 	except:
# 		raise Http404
# 	template_name = 'blog_post_detail.html'
# 	context = {"object": obj}
# 	return render(request, template_name, context)
class LoginUrlTestTemplateView(TemplateView):
    def get_context_data(self, *args, **kwargs):
        context = super(LoginUrlTestTemplateView, self).get_context_data(*args, **kwargs)
        print(context)
        context['login_url'] = settings.LOGIN_URL
        return context

# list all projects #
def blog_post_list_view(request):
	qs = BlogPost.objects.all().published() #filter(title__icontains='hello')
	if request.user.is_authenticated: 
		my_qs = BlogPost.objects.filter(user=request.user)# display all the blogs
		qs = (qs | my_qs).distinct() # post only once
	template_name = 'blog/list.html'
	context = {'object_list': qs} 
	return render(request, template_name, context)

#@login_required
#@staff_member_required
def blog_post_create_view(request):
	form = BlogPostModelForm(request.POST or None, request.FILES or None)
	if form.is_valid():
		obj = form.save(commit=False)
		obj.user = request.user
		obj.save()
		form = BlogPostModelForm()
	template_name = 'form.html'
	context = {
	"title": "Create a New Blog", 
	'form': form}
	return render(request, template_name, context)

# 1 object -> detail view #
def blog_post_detail_view(request, slug):
	obj = get_object_or_404(BlogPost, slug=slug)
	template_name = 'blog/detail.html'
	context = {"object": obj}
	return render(request, template_name, context) 
    
#@staff_member_required
def blog_post_update_view(request, slug):
	obj = get_object_or_404(BlogPost, slug=slug)
	form = BlogPostModelForm(request.POST or None, instance=obj)
	if form.is_valid():
		form.save()
	template_name = 'form.html'
	context = {'title': f'Update {obj.title}', 'form': form}
	return render(request, template_name, context) 

#@staff_member_required
def blog_post_delete_view(request, slug):
	obj = get_object_or_404(BlogPost, slug=slug)
	template_name = 'blog/delete.html'
	if request.method == 'POST':
		obj.delete()
		return redirect("/blog") #return to the page you want 
	context = {"object": obj}
	return render(request, template_name, context) 
  
    
    

